#include <iostream>
#include <cstring>
#include <cctype>

using namespace std;

#include "fraction.h"

#ifdef UNIMPLEMENTED_classFraction
#ifndef _MSC_FULL_VER // if not Visual Studio Compiler
#warning "Klasa jest do zaimplementowania. Instrukcja w pliku naglowkowym"
#else
#pragma message ("Klasa jest do zaimplementowania. Instrukcja w pliku naglowkowym")
#endif
#endif // UNIMPLEMENTED_classFraction

Fraction::Fraction() : numerator_(0), denominator_(1) {}

Fraction::Fraction(float numerator, float denominator) : numerator_(numerator),
                                                         denominator_(denominator) {}

float Fraction::getNumerator() const {
    return numerator_;
}

void Fraction::setNumerator(float numerator) {
    numerator_ = numerator;
}

void Fraction::setDenominator(float denominator) {
    Fraction::denominator_ = denominator;
}

float Fraction::getDenominator() const {
    return denominator_;
}

void Fraction::print() const {
    cout << to_string() << endl;
}

uint Fraction::removedFractions() {
    return removedFractions_;
}

void Fraction::save(ostream &os) const {
    os << to_string();
}

void Fraction::load(istream &is) {
    string representation;
    is >> representation;
    int delimiter_position = (int) representation.find('/');
    string numerator_s = representation.substr(0, delimiter_position);
    string denominator_s = representation.substr(
            delimiter_position+1, representation.size() - numerator_s.size()
    );

    numerator_ = stof(numerator_s);
    denominator_ = stof(denominator_s);

}

std::string Fraction::to_string() const {
    return std::to_string((int)numerator_) + "/" + std::to_string((int)denominator_);
}

const string &Fraction::getFractionName() const {
    return fractionName_;
}

Fraction::Fraction(float numerator, float denominator, string fractionName) : numerator_(numerator),
                                                                              denominator_(denominator),
                                                                              fractionName_(std::move(fractionName)) {}

uint Fraction::removedFractions_ = 0;
const float Fraction::invalidDenominatorValue = 0;

Fraction::~Fraction() {
    removedFractions_++;
}

float Fraction::getInvalidDenominatorValue() {
    return invalidDenominatorValue;
}
